# Lab 1.02 - Advanced API Proxy Development, Deployment and Testing

## Introduction

The objectives of this lab are listed below:

* Get yourself used to developing without using the Management UI wizards.
* Automate the deployment of the API proxy created in Lab 101. 
* Automate the setup of organization and environment configurations. 
* Write BDD tests using cucumber.js and apickli.

## Pre-requisites

The following software needs to be installed in your machine:

* JDK (1.8+)
* Maven (Preferably, the latest stable version)
* Node.js (Preferably, the latest stable version)
* IDE (Visual Studio, Atom, ...) + Extensions / Packages

## Setting Up the API Proxy Directory Structure

1. Create a directory named book-api-v1

2. Download the API proxy bundle for the proxy created in Lab 1.01 and place it in the directory that was created in the previous step.

3. Unzip the API proxy bundle inside the book-api-v1 directory and remove the archive. You should see a directory structure like the one shown below:

        +-- book-api-v1
            |-- apiproxy
                |-- policies
                |   |-- *.xml
                |-- proxies
                |   |-- default.xml
                |-- resources
                    |-- jsc
                    |-- *.js
                |-- targets
                |   |-- default.xml
            |-- book-api-v1.xml

NOTE: In order to save some time, we have not created the directory structure ourselves. In general, you will have to create this yourself, either manually or using a generator (yeoman, plop, slush, ...) and add the required policies, resources,... as you proceed with the implementation.

## Setting Up the Environment and Organization Configurations

1. Create the following directory structure inside the book-api-v1 directory:

        +-- book-api-v1
            |-- config
                |-- env
                |   |-- test
                |       |-- *.json
                |-- org
                    |-- *.json

2. Create the following files for the environment configuration inside the env/test directory

    * caches.json

            [
                {
                    "name": "book${deployment.suffix}-api-v1-response-cache",
                    "description": "Response cache for the Book API (v1)"
                }
            ]

    * kvms.json

            [
                {
                    "name": "book${deployment.suffix}-api-v1-configuration",
                    "entry": [
                        {
                            "name": "cacheEntryExpiry",
                            "value": "3600"
                        }
                    ]
                }
            ]

    * targetServers.json

            [{
                "name": "library${deployment.suffix}-api-v1",
                "host": "apigee-dev-training.appspot.com",
                "isEnabled": true,
                "port": 443,
                "sSLInfo": {
                    "clientAuthEnabled": "false",
                    "enabled": "true",
                    "ignoreValidationErrors": "false"
                }
            }]

3. Finally, create these other files inside the org directory:

    * apiProducts.jsom

            [
                {
                    "name": "BookAPIProduct${deployment.suffix}",
                    "displayName": "BookAPIProduct",
                    "description": "Book API Product",
                    "approvalType": "auto",
                    "environments": [
                        "test"
                    ],
                    "proxies": [
                        "book${deployment.suffix}-api-v1"
                    ]
                }
            ]

    * developer.json

            [
                {
                    "email": "john.doe@acme.com",
                    "firstName": "John",
                    "lastName": "Doe",
                    "userName": "john.doe"
                }
            ]

    * developerApps.json

            {
                "john.doe@acme.com": [
                    {
                        "apiProducts": [
                            "BookAPIProduct${deployment.suffix}"
                        ],
                        "callbackUrl": "http://callback",
                        "name": "TestBookApp",
                        "scopes": []
                    }
                ]
            }

## Setting Up BDD Testing

1. Create the following directory structure inside the book-api-v1 directory:

        +-- book-api-v1
            |-- test
                |-- integration
                    |-- features
                        |-- book-api-v1.feature
                        |-- step_definitions
                            |-- *.js
                    |-- package.json
                    |-- settings.json

2. Create the feature file with the scenarios that you would like to test. Use the step definitions provided by [apickli](https://github.com/apickli/apickli) to write test Scenarios. Find below the different things you need to test:

    * Get Books - Success
    * Get Books - Unauthorized, no API key was present
    * Search Books - Success
    * Search Books - Unauthorized, no API key was present
    * Search Books - Missing search term
    * Get Book By Id - Success
    * Search Books - Unauthorized, no API key was present
    * Get Book By Id - Book not found
    * API Proxy resouce not found

3. Install the Node.js dependencies required to run the tests:

        $ cd test/integration
        $ npm init
        $ npm install --save-dev apickli babel-core babel-preset-env cucumber cucumber-pretty es6-template-strings lodash node.extend path

4. Edit the package.json the babel config inside the package.json

        "babel": {
            "presets": [
                "env"
            ]
        }

    and modify the existing test script there with this one:

        "scripts": {
            "test": "node ./node_modules/cucumber/bin/cucumber.js --compiler js:babel-register --format node_modules/cucumber-pretty"
        }

5. Add the following files inside the test/integration/feature/step_definitions directory

    * init.js

            /*jshint esversion: 6 */

            import apickli from 'apickli';
            import {
                defineSupportCode
            } from 'cucumber';

            import template from 'es6-template-strings';
            import extend from 'node.extend';
            import path from 'path';
            import _ from 'lodash';

            var SETTINGS_FILENAME = 'settings.json';

            const org = process.env.ORG;
            const env = process.env.ENV || 'test';
            const deploymentSuffix = (typeof process.env.DEPLOYMENT_SUFFIX === 'undefined') ? '-' + process.env.USERNAME : process.env.DEPLOYMENT_SUFFIX;

            var tokens = {
                apigee: {
                    org: org,
                    env: env
                },
                deployment: {
                    suffix: deploymentSuffix
                }
            };

            const expand = (value, tokens) => {
                if (_.isArray(value)) {
                    return value.map(item => expand(item, tokens));
                } else if (_.isObject(value)) {
                    return _.mapValues(value, item => expand(item, tokens));
                } else {
                    return template(value, tokens);
                }
            };

            const loadSettings = (file) => {
                let settings = {};
                const data = require(file);
                    let defaultData = {};
                    if (data.default) {
                        defaultData = data.default;
                    }
                    settings = (data[org] && data[org][env]) ? extend(true, extend(true, {}, expand(defaultData, tokens)), expand(data[org][env], tokens)) : expand(defaultData, tokens);
                return settings;
            };

            const scenarioVars = loadSettings(path.resolve(process.cwd(), SETTINGS_FILENAME));

            defineSupportCode(function ({
                Before
            }) {

                Before(function () {
                    this.apickli = new apickli.Apickli(scenarioVars.scheme, scenarioVars.domain);
                    extend(this.apickli.scenarioVariables, scenarioVars);
                });
            });

    * apickli-gherkin.js

            module.exports = require('apickli/apickli-gherkin');

6. Create a configuration file named settings.json for the tests inside the test/integration directory with the following contents

        {
            "default": {
                "scheme": "https",
                "domain": "${apigee.org}-${apigee.env}.apigee.net/book${deployment.suffix}/v1"
            },
            "APIGEE-ORGANIZATION": {
                "test": {
                    "apikey": "API-KEY"
                }
            }
        }

    Replace APIGEE-ORGANIZATION with the name of your organization. We will set the value of the apikey at a later stage, once we have run the deployment script and the developer app has been created.

## Deploying and Testing using Maven

1. Configure a new Maven profile for Apigee in ~/.m2/settings.xml with your Apigee username and password and activate it. That way you will not need to enter your credentials every time you deploy an API proxy.

        <settings xmlns="http://maven.apache.org/SETTINGS/1.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0
                            https://maven.apache.org/xsd/settings-1.0.0.xsd">
            <profiles>
                <profile>
                    <id>public</id>
                    <properties>
                        <username>APIGEE-USERNAME</username>
                        <password>APIGEE-PASSWORD</password>
                    </properties>
                </profile>
            </profiles>
            <activeProfiles>
                <activeProfile>public</activeProfile>
            </activeProfiles>
        </settings>

2. Get the required Maven POM file from [here](./solution/book-api-v1/pom.xml) and place it in inside the book-api-v1 directory.

3. Deploy the API proxy running the command below.

        $ mvn install -Ptest -Dorg=APIGEE-ORGANIZATION

    Make sure to copy the consumer key from the standard output and set it as the API key in the test/integration/settings.json file.

    In case you would like to run the tests without deploying the proxy you can use the following command

        $ ORG=APIGEE-ORGANIZATION npm test

