# Lab 2.01 - Mocking with Node.js

## Introduction

The objective of this lab is to learn how to create a mock using apimocker Node.js package. We will be creating a mock of a user authentication service, that we using during Day 3 lab sessions.

## Instructions

1. Create a directory called mock-api and open your preferred editor (Atom, Visual Studio,...) in it.

2. Add your build script inside that root directory. For this exercise we will be using maven. Please get the required POM file from [here](./solution/mock-api/pom.xml). 

3. Create the following structure inside the mock-api directory:

        mock-api
        |-- apiproxy
        |   |-- proxies
        |   |-- targets
        |   |-- resources
        |       |-- node
        |-- test
            |-- integration
                |-- features
                    |-- step_definitions

4. Run the following commands inside the test/integration directory:

        $ npm init
        $ npm install --save-dev apickli babel-core babel-preset-env cucumber cucumber-pretty es6-template-strings lodash node.extend path

5. Edit the package.json that was generated and add the babel configuration:

        "babel": {
            "presets": [
                "env"
            ]
        }

6. Inside that same file, also make sure that the test script command is as follows:

        "scripts": {
            "test": "node node_modules/cucumber/bin/cucumber.js --compiler js:babel-register --format node_modules/cucumber-pretty"
        }

7. Create the feature file mock-api.feature inside mock-api/test/integration/features with the following content:

        Feature: Mock API tests
        
            Scenario: Success
                Given I set form parameters to
                    | parameter | value   |
                    | username  | user    |
                    | password  | valid   |
                When I POST to /authenticate
                Then response code should be 200
                And response body should be valid json
                And response body path $.id should be (.+)
                And response body path $.firstName should be (.+)
                And response body path $.lastName should be (.+)
                And response body path $.email should be (.+)
        
            Scenario: Invalid credentials
                Given I set form parameters to
                    | parameter | value   |
                    | username  | user    |
                    | password  | invalid |
                When I POST to /authenticate
                Then response code should be 401
                And response body should be valid json
                And response body path $.code should be unauthorized
                And response body path $.message should be User credentials are invalid

8. Create a file named init.js inside mock-api/test/integration/features/step_definitions directory with the following contents:

        /*jshint esversion: 6 */

        import apickli from 'apickli';
        import {
            defineSupportCode
        } from 'cucumber';

        import template from 'es6-template-strings';
        import extend from 'node.extend';
        import path from 'path';
        import _ from 'lodash';

        var SETTINGS_FILENAME = 'settings.json';

        const org = process.env.ORG;
        const env = process.env.ENV || 'test';
        const deploymentSuffix = (typeof process.env.DEPLOYMENT_SUFFIX === 'undefined') ? '-' + process.env.USERNAME : process.env.DEPLOYMENT_SUFFIX;

        var tokens = {
            apigee: {
                org: org,
                env: env
            },
            deployment: {
                suffix: deploymentSuffix
            }
        };

        const expand = (value, tokens) => {
            if (_.isArray(value)) {
                return value.map(item => expand(item, tokens));
            } else if (_.isObject(value)) {
                return _.mapValues(value, item => expand(item, tokens));
            } else {
                return template(value, tokens);
            }
        };

        const loadSettings = (file) => {
            let settings = {};
            const data = require(file);
                let defaultData = {};
                if (data.default) {
                    defaultData = data.default;
                }
                settings = (data[org] && data[org][env]) ? extend(true, extend(true, {}, expand(defaultData, tokens)), expand(data[org][env], tokens)) : expand(defaultData, tokens);
            return settings;
        };

        const scenarioVars = loadSettings(path.resolve(process.cwd(), SETTINGS_FILENAME));

        defineSupportCode(function ({
            Before
        }) {

            Before(function () {
                this.apickli = new apickli.Apickli(scenarioVars.scheme, scenarioVars.domain);
                extend(this.apickli.scenarioVariables, scenarioVars);
            });
        });

9. Create a file named apickli-gherkin.js inside mock-api/test/integration/features/step_definitions directory with the following contents:

        module.exports = require('apickli/apickli-gherkin');

10. Create a file called settings.json inside mock-api/test/integration directory with the contents provided below:

        {
            "default": {
                "scheme": "https",
                "domain": "${apigee.org}-${apigee.env}.apigee.net/mock${deployment.suffix}"
            }
        }

11. Create the API proxy descriptor mock-api.xml inside mock-api/apiproxy directory with the following content:

        <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
        <APIProxy name="mock-api">
            <Description>Mock API</Description>
        </APIProxy>

12. Create the ProxyEndpoint descriptor default.xml inside mock-api/apiproxy/proxies directory with the content below:

        <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
        <ProxyEndpoint name="default">
            <HTTPProxyConnection>
                <BasePath>/mock${deployment.suffix}</BasePath>
                <VirtualHost>secure</VirtualHost>
            </HTTPProxyConnection>
            <RouteRule name="default">
                <TargetEndpoint>default</TargetEndpoint>
            </RouteRule>
        </ProxyEndpoint>

13. Create the descriptor default.xml for the TargetEndpoint that we were referencing in the ProxyEndpoint descriptor. In our case the TargetEndpoint will be a Node.js application. Add an XML file with the following contents to the apiproxy/targets directory.

        <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
        <TargetEndpoint name="default">
            <ScriptTarget>
                <ResourceURL>node://app.js</ResourceURL>
            </ScriptTarget>
        </TargetEndpoint>

14. The next step will be to initialize our Node.js app inside mock-api/apiproxy/resources/node directory and install the required node dependencies.

        $ npm init
        $ npm install --save apimocker@0.5.1

15. Inside the mock-api/apiproxy/resources/node/node_modules directory create a file named zip.xml with the following content:

        <assembly xmlns="http://maven.apache.org/plugins/maven-assembly-plugin/assembly/1.1.2"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://maven.apache.org/plugins/maven-assembly-plugin/assembly/1.1.2 http://maven.apache.org/xsd/assembly-1.1.2.xsd">
            <id>node-dependencies</id>
            <formats>
                <format>zip</format>
            </formats>
            <includeBaseDirectory>false</includeBaseDirectory>
            <fileSets>
                <fileSet>
                    <directory>${project.root.dir}/apiproxy/resources/node/node_modules</directory>
                    <outputDirectory>node_modules</outputDirectory>
                    <excludes>
                        <exclude>nodemon/**</exclude>
                    </excludes>
                </fileSet>
            </fileSets>
        </assembly>

16. Create the Node.js app entry point, a file named app.js, inside mock-api/apiproxy/resources/node directory with the content below:

        var ApiMocker = require('apimocker');
        
        var options = {};
        
        var apiMocker = ApiMocker.createServer(options)
            .setConfigFile('config.json')
            .start();

17. Create a file named config.json inside the apiproxy/resources/node directory. This file is where we are going to configure the responses that our mock is going to send back to the invoking client. See below the content of the file:

        {
            "mockDirectory": "./mock",
            "quiet": false,
            "port": "8080",
            "latency": 50,
            "logRequestHeaders": false,
            "webServices": {
                "authenticate": {
                    "latency": 20,
                    "verbs": ["post"],
                    "switch": ["password"],
                    "responses": {
                        "post": {"httpStatus": 401, "mockFile": "error.json"}
                    },
                    "switchResponses": {
                        "passwordvalid": {"httpStatus": 200, "mockFile": "success.json"}
                    }
                }
            }
        }

18. Create the error and success mock responses inside mock-api/apiproxy/resources/node/mock:

    * mock-api/apiproxy/resources/node/mock/success.json

            {
                "id": "9532e495-bf0a-4554-b813-4cfc54e67976",
                "firstName": "John",
                "lastName": "Doe",
                "email": "john.doe@acme.com"
            }

    * mock-api/apiproxy/resources/node/mock/error.json

            {
                "code": "unauthorized",
                "message": "User credentials are invalid"
            }

19. Deploy and test the Mock API proxy using mvn

        $ mvn install -Ptest -Dorg=APIGEE-ORGANIZATION
