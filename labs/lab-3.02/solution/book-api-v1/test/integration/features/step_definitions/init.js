/*jshint esversion: 6 */

import apickli from 'apickli';
import {
    defineSupportCode
} from 'cucumber';

import template from 'es6-template-strings';
import extend from 'node.extend';
import path from 'path';
import _ from 'lodash';
import request from 'request-promise';

var SETTINGS_FILENAME = 'settings.json';

const org = process.env.ORG;
const env = process.env.ENV || 'test';
const deploymentSuffix = (typeof process.env.DEPLOYMENT_SUFFIX === 'undefined') ? '-' + process.env.USERNAME : process.env.DEPLOYMENT_SUFFIX;

var tokens = {
    apigee: {
        org: org,
        env: env
    },
    deployment: {
        suffix: deploymentSuffix
    }
};

const expand = (value, tokens) => {
    if (_.isArray(value)) {
        return value.map(item => expand(item, tokens));
    } else if (_.isObject(value)) {
        return _.mapValues(value, item => expand(item, tokens));
    } else {
        return template(value, tokens);
    }
};

const loadSettings = (file) => {
    let settings = {};
    const data = require(file);
    let defaultData = {};
    if (data.default) {
        defaultData = data.default;
    }
    settings = (data[org] && data[org][env]) ? extend(true, extend(true, {}, expand(defaultData, tokens)), expand(data[org][env], tokens)) : expand(defaultData, tokens);
    return settings;
};

const scenarioVars = loadSettings(path.resolve(process.cwd(), SETTINGS_FILENAME));

defineSupportCode(function ({
    Before,
    Given,
    When,
    Then
}) {

    Before(function () {
        this.apickli = new apickli.Apickli(scenarioVars.scheme, scenarioVars.domain);
        extend(this.apickli.scenarioVariables, scenarioVars);
    });

    Given(/^I have a valid access token$/, function (callback) {
        var self = this;
        request.post({
            uri: this.apickli.scenarioVariables.tokenEndpointUrl,
            auth: {
                user: this.apickli.scenarioVariables.clientId,
                pass: this.apickli.scenarioVariables.clientSecret
            },
            form: {
                grant_type: "client_credentials"
            },
            json: true
        }).then(body => {
            this.apickli.addRequestHeader('Authorization', `Bearer ${body.access_token}`);            
            callback();
        }).catch(error => {
            callback(error);
        });
    });
});