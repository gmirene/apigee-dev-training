/*jshint esversion: 6 */

import apickli from 'apickli';
import {
    defineSupportCode
} from 'cucumber';

import template from 'es6-template-strings';
import extend from 'node.extend';
import path from 'path';
import _ from 'lodash';

var SETTINGS_FILENAME = 'settings.json';

const org = process.env.ORG;
const env = process.env.ENV || 'test';
const deploymentSuffix = (typeof process.env.DEPLOYMENT_SUFFIX === 'undefined') ? '-' + process.env.USER : process.env.DEPLOYMENT_SUFFIX;

var tokens = {
    apigee: {
        org: org,
        env: env
    },
    deployment: {
        suffix: deploymentSuffix
    }
};

const expand = (value, tokens) => {
    if (_.isArray(value)) {
        return value.map(item => expand(item, tokens));
    } else if (_.isObject(value)) {
        return _.mapValues(value, item => expand(item, tokens));
    } else {
        return template(value, tokens);
    }
};

const loadSettings = (file) => {
    let settings = {};
    const data = require(file);
        let defaultData = {};
        if (data.default) {
            defaultData = data.default;
        }
        settings = (data[org] && data[org][env]) ? extend(true, extend(true, {}, expand(defaultData, tokens)), expand(data[org][env], tokens)) : expand(defaultData, tokens);
    return settings;
};

const scenarioVars = loadSettings(path.resolve(process.cwd(), SETTINGS_FILENAME));

defineSupportCode(function ({
    Before, Given, Then, When
}) {

    Before(function () {
        this.apickli = new apickli.Apickli(scenarioVars.scheme, scenarioVars.domain);
        extend(this.apickli.scenarioVariables, scenarioVars);
    });

    Given(/^I have a valid access token and refresh token for app credentials (.*) and (.*) and user credentials (.*) and (.*)$/, function(clientId, clientSecret, username, password, callback) {
        var self = this;
        this.apickli.addHttpBasicAuthorizationHeader(clientId, clientSecret);
        var formParameters = [
            {
                parameter: "grant_type",
                value: "password"
            },
            {
                parameter: "username",
                value: username
            },
            {
                parameter: "password",
                value: password
            }
        ];
        this.apickli.setFormParameters(formParameters);
        this.apickli.post('/token', function(error, response) {
            if (error) {
                callback(new Error(error));
            }
            self.apickli.storeValueInScenarioScope('refreshToken', JSON.parse(response.body).refresh_token);
            callback();
        });
    });

    When(/^I try to refresh the access token$/, function(callback) {
        const scenarioVars = this.apickli.scenarioVariables;
        var formParameters = [
            {
                parameter: "grant_type",
                value: "refresh_token"
            },
            {
                parameter: "refresh_token",
                value: this.apickli.scenarioVariables.refreshToken
            }
        ];
        this.apickli.setFormParameters(formParameters);
        this.apickli.post('/token', function(error, response) {
            if (error) {
                callback(new Error(error));
            }
            callback();
        });
    });

});
